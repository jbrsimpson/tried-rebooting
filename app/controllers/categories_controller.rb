class CategoriesController < ApplicationController
  before_action :require_user, except: [:index, :show]
  before_action :require_admin, only: [:edit, :update, :destroy]
  before_action :set_category, only: [:show, :edit, :update, :destroy]

  def index
    @categories = Category.where(parent_id: nil)
  end

  def show
  end

  def new
    @categories = Category.where(parent_id: nil)
  end

  def new_name
    @parent = Category.find_by slug: params[:parent]
    @category = Category.new(parent: @parent)
  end

  def create
    @parent = Category.find_by slug: params[:category][:parent]
    @category = Category.new(category_params)
    @category.parent = @parent
    if @category.save
      redirect_to new_problem_form_path(category: @category.slug)
    else
      flash[:danger] = "Something went wrong"
      render :new_name
    end
  end

  def edit
  end

  def update
    if @category.update(category_params)
      flash[:info] = "Category updated successfully"
      redirect_to categories_path
    else
      flash[:danger] = "Something went wrong"
      render :edit
    end
  end

  def destroy
    if @category.problems.any?
      flash[:danger] = "Please delete these problems first"
      redirect_to category_path(@category)
    else
      @category.destroy
      redirect_to categories_path
    end
  end

  private

  def category_params
    params.require(:category).permit(:name)
  end

  def set_category
    @category = Category.find_by slug: params[:id]
  end 
end