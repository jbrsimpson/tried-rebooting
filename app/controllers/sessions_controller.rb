class SessionsController < ApplicationController
  def new
    redirect_to problems_path if logged_in?
  end

  def create
    user = User.find_by(username: params[:username])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      flash[:info] = "Login Successful"
      redirect_to problems_path
    else
      flash[:danger] = "Authentication Failed"
      render :new
    end
  end

  def destroy
    session[:user_id] = nil
    flash[:info] = "Logged out"
    redirect_to problems_path
  end 
end