class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :current_user, :logged_in?

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def logged_in?
    !!current_user
  end

  def require_user
    unless logged_in?
      flash[:danger] = "Only a registered user can do that"
      redirect_to login_path
    end
  end

  def require_admin
    unless logged_in? && current_user.admin
      flash[:danger] = "Only an admin can do that"
      redirect_to login_path
    end
  end

  def require_right_user
    unless logged_in? && (current_user == @user || current_user.admin)
      flash[:danger] = "You can't do that"
      redirect_to problems_path
    end
  end

  def require_creator
    unless logged_in? && (current_user == @problem.user || current_user.admin)
      flash[:danger] = "Only the creator of the problem can do that"
      redirect_to problem_path(@problem)
    end
  end
end
