class PagesController < ApplicationController
  def home

  end

  def search
    @term = params[:term]
    @obj = params[:obj]
    case @obj
    when "problem"
      @problems = Problem.search(params[:term])
    when "category"
      @categories = Category.search(params[:term])
    when "user"
      @users = User.search(params[:term])
    end
  end

  def test
    @roots = Category.where(parent_id: nil)
  end
end