class UsersController < ApplicationController
  before_action :set_user, only: [:show, :make_admin, :edit, :update, :destroy]
  before_action :require_admin, only: [:index, :make_admin]
  before_action :require_right_user, only: [:edit, :update, :destroy]

  def index
    @users = User.all
  end

  def show
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:info] = "Please log in"
      redirect_to login_path
    else
      flash[:danger] = "Invalid input"
      render :new
    end
  end

  def edit
  end

  def update
    if @user.update(user_params)
      flash[:info] = "Account updated"
      redirect_to user_path(@user)
    else
      flash[:danger] = "Invalid input"
      render :edit
    end
  end

  def make_admin
    @user.update(role: "admin")
    flash[:success] = "#{@user.username} has been granted admin privileges"
    redirect_to users_path
  end

  def destroy
    if current_user == @user
      session[:user_id] = nil
      @user.destroy
      flash[:success] = "Sorry to see you go"
      redirect_to problems_path
    else
      @user.destroy
      flash[:success] = "User deleted"
      redirect_to users_path
    end
  end
  
  private

  def user_params
    params.require(:user).permit(:username, :email, :password, :role)
  end

  def set_user
    @user = User.find_by slug: params[:id]
  end
end