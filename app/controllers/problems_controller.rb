class ProblemsController < ApplicationController
  before_action :set_problem, only: [:show, :edit, :update, :destroy]
  before_action :require_user, except: [:index, :show]
  before_action :require_creator, only: [:edit, :update, :destroy]

  def index
    @problems = Problem.all
  end

  def show
  end

  def new
    @categories = Category.where(parent_id: nil)
  end

  def new_form
    @category = Category.find_by slug: params[:category]
    @problem = Problem.new(category: @category)
  end

  def create
    @problem = Problem.new(problem_params)
    @problem.category = Category.find_by slug: params[:problem][:category]
    @problem.user = current_user
    if @problem.save
      flash[:success] = "Thanks for sharing!"
      redirect_to problem_path(@problem)
    else
      flash[:danger] = "Invalid input"
      render :new_form
    end
  end

  def edit
  end

  def update
    @category = Category.find_by slug: params[:problem][:category]
    if @problem.update(problem_params) && @problem.update(category: @category)
      flash[:success] = "Changes saved"
      redirect_to problem_path(@problem)
    else
      flash[:danger] = "Invalid input"
      render :edit
    end
  end

  def destroy
    @problem.destroy
    redirect_to problems_path
  end

  private

  def problem_params
    params.require(:problem).permit(:title, :description, :error_message, :solution, :user)
  end

  def set_problem
    @problem = Problem.find_by slug: params[:id]
  end
end