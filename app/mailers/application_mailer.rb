class ApplicationMailer < ActionMailer::Base
  default from: "ben@triedrebooting.com"
  default to: Proc.new { Admin.pluck(:email) }

  def forgot_password(user)
    @user = user
    mail(to: @user.email, subject: "Password Reset")
  end

  def new_category(problem)
    @problem = problem
    mail(subject: "New category at TriedRebooting")
  end
end
