class Category < ActiveRecord::Base
  include Sluggable
  sluggable_column :name

  validates :name, presence: true, uniqueness: true

  default_scope { order('name') }

  belongs_to :parent, foreign_key: "parent_id", class_name: "Category"
  has_many :children, foreign_key: "parent_id", class_name: "Category"
  has_many :problems

  def self.search(term)
    return [] if term.blank?
    where("name LIKE ?", "%#{term}%")
  end
end