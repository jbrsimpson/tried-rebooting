class User < ActiveRecord::Base
  include Sluggable
  sluggable_column :username

  has_secure_password validations: false

  validates :username, presence: true, uniqueness: true, length: {minimum: 3}
  validates :email, presence: true, uniqueness: true, email: true
  validates :password, presence: true, on: :create, length: {minimum: 5}

  default_scope { order "username" }

  has_many :problems

  def self.search(term)
    return [] if term.blank?
    where("username LIKE ?", "%#{term}%")
  end

  def admin
    role == "admin"
  end
end