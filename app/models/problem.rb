class Problem < ActiveRecord::Base
  include Sluggable
  sluggable_column :title

  validates :title, presence: true, uniqueness: true, length: {minimum: 10}
  validates :description, presence: true, length: {minimum: 20}
  validates :solution, presence: true, length: {minimum: 10}
  validates :category, presence: true
  validates :user, presence: true

  default_scope { order('created_at DESC') }

  belongs_to :category
  belongs_to :user

  def self.search(term)
    return [] if term.blank?
    where("title LIKE ?", "%#{term}%").order(created_at: :desc)
  end
end