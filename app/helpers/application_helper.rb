module ApplicationHelper
  require 'action_view'
  include ActionView::Helpers::DateHelper

  def get_age(obj_date)
    distance_of_time_in_words(obj_date, DateTime.now)
  end

  def parent_icon(category)
    if category.parent.nil?
      return haml_tag "span.glyphicon.glyphicon-hdd" if category == Category.find_by(name: "Hardware")
      return haml_tag "span.glyphicon.glyphicon-console" if category == Category.find_by(name: "Operating System")
      return haml_tag "span.glyphicon.glyphicon-list-alt" if category == Category.find_by(name: "Application")
    else
      parent_icon(category.parent)
    end
  end

  def expand_categories_list(category)
    category.children.each do |category|
      if category.children.any?
        haml_tag "a.list-group-item", category.name, { "data-toggle" => "collapse", "data-parent" => "#accordion", "href" => "#collapse-#{category.slug}" }
        haml_tag "div.collapse.list-group-submenu#collapse-#{category.slug}" do
          expand_categories_list(category)
        end
      else
        haml_tag "a.list-group-item", category.name, { "href" => "/categories/#{category.slug}" }
      end
    end
  end

  # def expand_categories_panel(category)
  #   category.children.each do |category|
  #     if category.children.any?
  #       haml_tag "panel-group#a-#{category.slug}" do
  #         haml_tag "a.panel.panel-default.panel-title", category.name, { "data-toggle" => "collapse", "data-parent" => "#a-#{category.slug}", "href" => "#collapse-#{category.slug}" }
  #         haml_tag "div.collapse.#collapse-#{category.slug}" do
  #           expand_categories2(category)
  #         end
  #       end
  #     else
  #       haml_tag "a.panel.panel-default.panel-title", category.name, { "href" => "/categories/#{category.slug}" }
  #     end
  #   end
  # end
end
