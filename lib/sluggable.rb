module Sluggable  
  extend ActiveSupport::Concern

  included  do
    before_save :generate_slug
    class_attribute :slug_column
  end

  def generate_slug
    new_slug = to_slug(self.send(self.class.slug_column.to_sym))
    other = self.class.find_by(slug: new_slug)
    count = 2
    while other && other !=self
      new_slug = append_suffix(new_slug, count)
      other = self.class.find_by(slug: new_slug)
      count += 1
    end
    self.slug = new_slug
  end

  def append_suffix(str, count)
    if str.split("-").last.to_i != 0
      return str.split("-").slice(0...-1).join("-") + "-" + count.to_s
    else
      return str + "-" + count.to_s
    end
  end

  def to_slug(str)
    str = str.strip
    str = str.gsub /\s*[^A-Za-z0-9]\s*/, "-"
    str = str.gsub /-+/, "-"
    str = str.downcase
  end

  def to_param
    self.slug
  end

  module ClassMethods
    def sluggable_column(column)
      self.slug_column = column
    end
  end
end