require "rails_helper"

describe SessionsController do
  describe "POST create" do
    let(:user) { Fabricate(:user) }

    context "with valid input" do 
      before { post :create, username: user.username, password: user.password }
      
      it("logs the user in") { expect(session[:user_id]).to eq(user.id) }
      it("redirects to problems path") { expect(response).to redirect_to problems_path }
    end

    context 'with invalid input' do
      before { post :create, username: user.username, password: user.password + "1" }

      it("doesn't log the user in") { expect(session[:user_id]).not_to eq(user.id) }
      it("renders new") { expect(response).to render_template :new }
    end
  end

  describe "DELETE destroy" do
    it "logs the user out" do
      delete :destroy
      expect(session[:user_id]).to eq(nil)
    end
  end
end