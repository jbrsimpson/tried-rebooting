require "rails_helper"

describe CategoriesController do
  describe "GET index" do
    it "sets @categories" do
      category = Fabricate(:category)
      get :index
      expect(assigns(:categories)).to eq([category])
    end
  end

  describe "GET show" do
    let(:category) { Fabricate(:category) }

    it "sets @category" do 
      get :show, id: category.slug
      expect(assigns(:category)).to eq(category)
    end
  end

  describe "GET new" do
    set_user
    it_behaves_like("require_user") { let(:action) { get :new } }
    let(:category) { Fabricate(:category) }

    it "sets @categories to roots" do
      child1 = Fabricate(:category, parent: category)
      child2 = Fabricate(:category, parent: category)
      get :new
      expect(assigns(:categories)).to eq([category])
    end
  end

  describe "GET new_name" do
    set_user
    it_behaves_like("require_user") { let(:action) { get :new_name } }
    let(:category) { Fabricate(:category) }

    it "sets @parent" do
      get :new_name, parent: category.slug
      expect(assigns(:parent)).to eq(category)
    end

    it "makes a new category" do
      get :new_name, parent: category.slug
      expect(assigns(:category)).to be_a_new(Category)
    end

    it "associates new category with parent" do
      get :new_name, parent: category.slug
      expect(assigns(:category).parent).to eq(category)
    end
  end

  describe "POST create" do
    set_user
    it_behaves_like("require_user") { let(:action) { post :create } }

    context "with valid input" do
      let(:category) { Fabricate(:category) }
      before { post :create, category: Fabricate.attributes_for(:category, parent: category) }
      it("creates the new category") { expect(Category.count).to eq(2) }
      it("redirects to new_problem_form_path") do 
        expect(response).to redirect_to new_problem_form_path(category: assigns(:category).slug)
      end
    end

    context "with invalid input" do
      before { post :create, category: { name: "" } }
      it("does not create the new category") { expect(Category.count).to eq(0) }
      it("renders new_name") { expect(response).to render_template :new_name }
    end
  end

  describe "GET edit" do
    set_admin
    it_behaves_like("require_admin") { let(:action) { get :edit, id: 0 } }

    it 'sets @category' do
      category = Fabricate(:category)
      get :edit, id: category.slug
      expect(assigns(:category)).to eq(category)
    end
  end

  describe "PATCH update" do
    set_admin
    it_behaves_like("require_admin") { let(:action) { patch :update, id: 0 } }
    let(:category) { Fabricate(:category, name: "Old Name") }

    context "with valid input" do
      before { patch :update, id: category.slug, category: { name: "New name" } }
      it('updates the category') { expect(category.reload.name).to eq("New name") }
      it("redirects to categories_path") { expect(response).to redirect_to categories_path }
    end

    context 'with invalid input' do
      before { patch :update, id: category.slug, category: { name: "" } }
      it("doesn't update the category") { expect(category.reload.name).not_to eq("") }
      it("renders edit") { expect(response).to render_template :edit }
    end
  end

  describe "DELETE destroy" do
    set_admin
    it_behaves_like("require_admin") { let(:action) { delete :destroy, id: 0 } }
    let(:category) { Fabricate(:category) }

    context "with no problems in the category" do
      before { delete :destroy, id: category.slug }
      it("destroys the category") { expect(Category.count).to eq(0) }
      it('redirects to categories path') { expect(response).to redirect_to categories_path }
    end

    context "with listings in the category" do
      before do
        Fabricate(:problem, category: category)
        delete :destroy, id: category.slug
      end

      it('does not destroy the category') { expect(Category.count).to eq(1) }
      it('redirects to category path') { expect(response).to redirect_to category_path(category) }
    end
  end
end