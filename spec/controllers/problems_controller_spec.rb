require "rails_helper"

describe ProblemsController do
  describe "GET index" do
    it "sets @problems" do
      problem = Fabricate(:problem)
      get :index
      expect(assigns(:problems)).to eq([problem])
    end
  end

  describe "GET show" do
    before { get :show, id: problem.slug }
    let(:problem) { Fabricate(:problem) }

    it('sets @problem') { expect(assigns(:problem)).to eq(problem) }
  end

  describe "GET new" do  
    set_user 
    it_behaves_like("require_user") { let(:action) { get :new } }
    let(:category) { Fabricate(:category) }
    before { get :new}

    it('sets @categories') { expect(assigns(:categories)).to eq([category]) }
  end

  describe "GET new_form" do  
    set_user 
    it_behaves_like("require_user") { let(:action) { get :new } }
    let(:category) { Fabricate(:category) }
    before { get :new_form, category: category }
    
    it('sets @category') { expect(assigns(:category)).to eq(category) }
    it("makes a new problem") { expect(assigns(:problem)).to be_a_new(Problem) }
    it("associates category with new problem") { expect(assigns(:problem).category).to eq(category) }
  end

  describe "POST create" do
    set_user 
    it_behaves_like("require_user") { let(:action) { post :create } }

    context "with valid input" do
      before { post :create, problem: Fabricate.attributes_for(:problem, category: Fabricate(:category).slug) }
      
      it("creates the new problem") { expect(Problem.count).to eq(1) }
      it("associates the user") { expect(Problem.first.user).to eq(current_user) }
      it("redirects to problem path") { expect(response).to redirect_to problem_path(Problem.first) }
    end

    context "with invalid input" do
      before { post :create, problem: { title: "Something" } }
      
      it("does not create the new problem") { expect(Problem.count).to eq(0) }
      it("renders new_form") { expect(response).to render_template :new_form }
    end
  end

  describe "GET edit" do
    set_user 
    it_behaves_like("require_creator") { let(:action) { get :edit, id: Fabricate(:problem).slug } }

    it 'sets @problem' do
      problem = Fabricate(:problem, user: current_user)
      get :edit, id: problem.slug
      expect(assigns(:problem)).to eq(problem)
    end
  end

  describe "PATCH update" do
    set_user 
    it_behaves_like("require_creator") { let(:action) { patch :update, id: Fabricate(:problem).slug } }
    let(:problem) { Fabricate(:problem, user: current_user) }

    context "with valid input" do
      before { patch :update, id: problem.slug, problem: { title: "A fancy new title", category: problem.category.slug } }

      it('updates the problem') { expect(problem.reload.title).to eq("A fancy new title") }
      it("redirects to the problem path") { expect(response).to redirect_to problem_path(problem.reload) }
    end

    context 'with invalid input' do
      before { patch :update, id: problem.slug, problem: { title: "" } }
      it("doesn't update the problem") { expect(problem.reload.title).not_to eq("") }
      it("renders the edit template") { expect(response).to render_template :edit }
    end
  end

  describe 'DELETE destroy' do
    set_user
    it_behaves_like("require_creator") { let(:action) { delete :destroy, id: Fabricate(:problem).slug } }

    before { delete :destroy, id: Fabricate(:problem, user: current_user).slug }

    it("destroys the problem") { expect(Problem.count).to eq(0) }
    it("redirects to problems path") { expect(response).to redirect_to problems_path }
  end
end