require "rails_helper"

describe PagesController do
  describe "POST search" do
    it "sets @term" do
      post :search, term: "search"
      expect(assigns(:term)).to eq("search")
    end

    it "sets @obj" do
      post :search, obj: "problem"
      expect(assigns(:obj)).to eq("problem")
    end

    it "sets @problems when obj is problem" do
      problem = Fabricate(:problem)
      post :search, obj: "problem", term: problem.title
      expect(assigns(:problems)).to eq([problem])
    end

    it "sets @categories when obj is category" do
      category = Fabricate(:category)
      post :search, obj: "category", term: category.name
      expect(assigns(:categories)).to eq([category])
    end

    it "sets @users when obj is user" do
      user = Fabricate(:user)
      post :search, obj: "user", term: user.username
      expect(assigns(:users)).to eq([user])
    end
  end
end