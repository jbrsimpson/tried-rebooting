require "rails_helper"

describe UsersController do
  describe "GET index" do
    set_admin
    it_behaves_like("require_admin") { let(:action) { get :index } }
    let(:user) { Fabricate(:user, username: "aaaaa") }
    before { get :index }

    it("sets users") { expect(assigns(:users)).to eq([user, current_user]) }
  end

  describe "GET show" do
    let(:user) { Fabricate(:user) }
    before { get :show, id: user.slug }

    it("sets user") { expect(assigns(:user)).to eq(user) }
  end

  describe "GET new" do
    it "sets user" do 
      get :new
      expect(assigns(:user)).to be_a_new(User)
    end
  end

  describe "POST create" do
    context "with valid input" do 
      before { post :create, user: Fabricate.attributes_for(:user) }

      it("creates the user") { expect(User.count).to eq(1) }
      it("redirects to login path") { expect(response).to redirect_to login_path }
    end

    context 'with invalid input' do
      before { post :create, user: { username: "" } }

      it("doesn't create the user") { expect(User.count).to eq(0) }
      it("renders new") { expect(response).to render_template :new }
    end
  end

  describe "GET edit" do
    set_user
    it_behaves_like("require_right_user") { let(:action) { get :edit, id: Fabricate(:user).slug } }

    it("sets user") do
      get :edit, id: current_user.slug
      expect(assigns(:user)).to eq(current_user)
    end
  end

  describe "PATCH update" do
    set_user
    it_behaves_like("require_right_user") { let(:action) { patch :update, id: Fabricate(:user).slug } }

    context "with valid input" do
      before { patch :update, id: current_user.slug, user: { username: "New name" } }
      it("updates the user info") { expect(current_user.reload.username).to eq("New name") }
      it("redirects to user path") { expect(response).to redirect_to user_path(current_user.reload) }
    end

    context 'with invalid input' do
      before { patch :update, id: current_user.slug, user: { username: "" } }

      it("doesn't update the user info") { expect(current_user.reload.username).not_to eq("") }
      it("renders edit") { expect(response).to render_template :edit }
    end
  end

  describe "PATCH make_admin" do
    set_admin
    it_behaves_like("require_admin") { let(:action) { patch :make_admin, id: 0 } }
    let(:user) { Fabricate(:user) }
    before { patch :make_admin, id: user.slug }

    it("makes the user an admin") { expect(user.reload.role).to eq("admin") }
    it("redirects to users path") { expect(response).to redirect_to users_path }
  end

  describe "DELETE destroy" do
    it_behaves_like("require_right_user") { let(:action) { delete :destroy, id: Fabricate(:user).slug } }

    context "when user is current_user" do
      set_user
      before { delete :destroy, id: current_user.slug }

      it("destroys the user") { expect(User.count).to eq(0) }
      it("redirects to problems path") { expect(response).to redirect_to problems_path }
    end

    context "when admin is deleting user" do
      set_admin
      let(:user) { Fabricate(:user) }
      before { delete :destroy, id: user.slug }

      it("destroys the user") { expect(User.count).to eq(1) }
      it("redirects to users path") { expect(response).to redirect_to users_path }
    end
  end
end