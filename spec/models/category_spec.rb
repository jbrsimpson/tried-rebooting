require "rails_helper"

describe Category do
  it_behaves_like ("has_a_slug") { let(:model) { Fabricate(:category) } }

  it { should have_many(:problems) }
  it { should have_many(:children) }
  it { should belong_to(:parent) }

  describe ".search" do
    it "returns matching results" do
      category = Fabricate(:category, name: "search me")
      other_category = Fabricate(:category, name: "not this one")
      expect(Category.search(category.name)).to eq([category])
    end

    it "returns [] if blank" do
      expect(Category.search("")).to eq([])
    end
  end
end