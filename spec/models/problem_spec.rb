require "rails_helper"

describe Problem do
  it_behaves_like ("has_a_slug") { let(:model) { Fabricate(:problem) } }

  it { should belong_to(:category) }
  it { should belong_to(:user) }

  describe ".search" do
    it "returns matching results" do
      problem = Fabricate(:problem, title: "search for me")
      other_problem = Fabricate(:problem, title: "not this one")
      expect(Problem.search(problem.title)).to eq([problem])
    end

    it "returns [] if blank" do
      expect(Problem.search("")).to eq([])
    end
  end
end