require "rails_helper"

describe User do
  it_behaves_like ("has_a_slug") { let(:model) { Fabricate(:user) } }

  it { should have_many(:problems) }
  it { should have_secure_password }

  describe ".search" do
    it "returns matching results" do
      user = Fabricate(:user, username: "search me")
      other_user = Fabricate(:user, username: "not this one")
      expect(User.search(user.username)).to eq([user])
    end

    it "returns [] if blank" do
      expect(User.search("")).to eq([])
    end
  end

  describe "#admin" do
    it "returns true if admin" do
      expect(Fabricate(:admin).admin).to be_truthy
    end

    it "returns false unless admin" do
      expect(Fabricate(:user).admin).to be_falsy
    end
  end
end