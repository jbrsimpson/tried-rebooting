class CreateProblems < ActiveRecord::Migration
  def change
    create_table :problems do |t|
      t.integer :user_id, :category_id
      t.string :title, :slug
      t.text :description, :error_message, :solution
      t.timestamps
    end
  end
end
