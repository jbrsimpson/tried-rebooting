User.create(username: "admin", email: "admin@admin.com", password: "admin", role: "admin")

hardware = Category.create(name: "Hardware")
os = Category.create(name: "Operating System")
app = Category.create(name: "Application")

comp = Category.create(name: "Computers", parent: hardware)

laptop = Category.create(name: "Laptops", parent: comp)
Category.create(name: "Asus S56ca", parent: laptop)

desktop = Category.create(name: "Desktops", parent: comp)
Category.create(name: "Dell Desktop", parent: desktop)

mob = Category.create(name: "Mobile", parent: hardware)

sam = Category.create(name: "Samsung Galaxy", parent: mob)
Category.create(name: "Samsung Galaxy S4", parent: sam)
Category.create(name: "Samsung Galaxy S5", parent: sam)
Category.create(name: "Samsung Galaxy S6", parent: sam)

iphone = Category.create(name: "iPhone", parent: mob)
Category.create(name: "iPhone 5", parent: iphone)
Category.create(name: "iPhone 5s", parent: iphone)

eread = Category.create(name: "eReader", parent: hardware)
Category.create(name: "Kobo Glo", parent: eread)

windows = Category.create(name: "Windows", parent: os)
Category.create(name: "Windows 7", parent: windows)
Category.create(name: "Windows 8", parent: windows)
Category.create(name: "Windows 10", parent: windows)

linux = Category.create(name: "Linux", parent: os)

mint = Category.create(name: "Linux Mint", parent: linux)
Category.create(name: "Linux Mint 16", parent: mint)
Category.create(name: "Linux Mint 17", parent: mint)

ubuntu = Category.create(name: "Ubuntu", parent: linux)
Category.create(name: "Ubuntu 12.04", parent: ubuntu)
Category.create(name: "Ubuntu 12.10", parent: ubuntu)
Category.create(name: "Ubuntu 14.04", parent: ubuntu)
Category.create(name: "Ubuntu 14.10", parent: ubuntu)

osx = Category.create(name: "OSX", parent: os)
Category.create(name: "Maverick", parent: osx)
Category.create(name: "El Capitan", parent: osx)

Category.create(name: "BIOS", parent: os)

outlook = Category.create(name: "Outlook", parent: app)
Category.create(name: "Outlook 2007", parent: outlook)
Category.create(name: "Outlook 2010", parent: outlook)
Category.create(name: "Outlook 2013", parent: outlook)

word = Category.create(name: "Word", parent: app)
Category.create(name: "Word 2007", parent: word)
Category.create(name: "Word 2010", parent: word)
Category.create(name: "Word 2013", parent: word)

photo = Category.create(name: "Photoshop", parent: app)
Category.create(name: "Photoshop CS6", parent: photo)
Category.create(name: "Photoshop CC", parent: photo)

Category.create(name: "Thunderbird", parent: app)
Category.create(name: "Steam", parent: app)
Category.create(name: "Firefox", parent: app)