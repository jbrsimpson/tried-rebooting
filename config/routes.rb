Rails.application.routes.draw do  
  root to: "pages#home"

  get "problems/new_form", to: "problems#new_form", as: "new_problem_form"
  get "categories/new_name", to: "categories#new_name", as: "new_category_name"
  patch "/users/:id/admin", to: "users#make_admin", as: "make_admin"

  post "/search", to: "pages#search"

  resources :problems
  resources :categories
  resources :users
  
  get "/register", to: "users#new"
  get "/login", to: "sessions#new"
  post "/login", to: "sessions#create"
  get "/logout", to: "sessions#destroy"

  if Rails.env.development?
    mount LetterOpenerWeb::Engine, at: "/emails"
    get "/test", to: "pages#test"
  end
end